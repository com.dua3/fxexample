FxExample
=========

This is a simple example Application that can be used as a starting point to create new applications with the FxApplication framework.

Prerequisites
-------------

Since the goal is to take full advantage of the module system, recent versions of tools are required to build:

 - JDK 16 is required because it is the first JDK version with a non-incubating `jpackage` implementation. 
 - Gradle 7.0 or newer is required because it is the first version with out-of-the-box support for the Java module system.
 - Currently a non-public
