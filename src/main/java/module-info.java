module com.dua3.fxexample {
    exports com.dua3.fxexample;
    opens com.dua3.fxexample;
    
    requires java.logging;

    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;

    requires dua3_fx.application;

    requires dua3_utility;
}
