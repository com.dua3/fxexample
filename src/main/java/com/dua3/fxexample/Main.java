package com.dua3.fxexample;

import com.dua3.fx.application.FxLauncher;

import java.io.IOException;

public final class Main {

    private Main() {
    }

    public static void main(String[] args) throws IOException {
        FxLauncher.launch(App.class, args);
    }

}
