package com.dua3.fxexample;

import com.dua3.fx.application.FxController;
import com.dua3.fx.application.FxDocument;

import java.util.Collections;
import java.util.List;

public class Controller extends FxController<App,Controller> {

    @Override
    public List<? extends FxDocument> dirtyDocuments() {
        return Collections.emptyList();
    }

}
