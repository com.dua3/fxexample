package com.dua3.fxexample;

import com.dua3.fx.application.FxApplication;
import com.dua3.utility.lang.LangUtil;

import java.net.URL;

public class App extends FxApplication<App,Controller> {

    static final String APP_VERSION = "0.0.1";

    @Override
    protected URL getFxml() {
        return LangUtil.getResourceURL(getClass(), "App.fxml");
    }


    @Override
    public void showPreferencesDialog() {

    }

    @Override
    public String getVersion() {
        return APP_VERSION;
    }
    
}
