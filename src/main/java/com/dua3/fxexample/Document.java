package com.dua3.fxexample;

import com.dua3.fx.application.FxDocument;

import java.io.IOException;
import java.net.URI;

public class Document extends FxDocument {
    public Document(URI location) {
        super(location);
    }

    @Override
    protected void write(URI uri) throws IOException {
        
    }
}
